apollo.datasets
==================================================
.. automodule:: apollo.datasets
	:no-members:


apollo.datasets.nam
--------------------------------------------------
.. automodule:: apollo.datasets.nam
	:no-members:

.. autosummary::
	:toctree: stubs

	open
	open_local
	open_range
	NamLoader
	CacheMiss


apollo.datasets.ga_power
--------------------------------------------------
.. automodule:: apollo.datasets.ga_power
	:no-members:

.. autosummary::
	:toctree: stubs

	interval
	open_mb007


apollo.datasets.solar
--------------------------------------------------
.. automodule:: apollo.datasets.solar
	:no-members:

.. autosummary::
	:toctree: stubs

	ATHENS_LATLON
	PLANAR_FEATURES
	SolarDataset
