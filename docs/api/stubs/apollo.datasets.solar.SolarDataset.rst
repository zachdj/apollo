apollo.datasets.solar.SolarDataset
==================================

.. currentmodule:: apollo.datasets.solar

.. autoclass:: SolarDataset

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~SolarDataset.__init__
      ~SolarDataset.tabular
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~SolarDataset.labels
      ~SolarDataset.shape
   
   