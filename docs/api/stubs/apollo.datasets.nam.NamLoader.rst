apollo.datasets.nam.NamLoader
=============================

.. currentmodule:: apollo.datasets.nam

.. autoclass:: NamLoader

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~NamLoader.__init__
      ~NamLoader.download
      ~NamLoader.grib_path
      ~NamLoader.grib_url
      ~NamLoader.load_grib
      ~NamLoader.nc_path
      ~NamLoader.normalize_grib
      ~NamLoader.open
      ~NamLoader.open_gribs
      ~NamLoader.open_nc
      ~NamLoader.open_range
   
   

   
   
   