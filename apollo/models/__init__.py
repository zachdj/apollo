# we would like `import apollo.models` to import all instantiable models
from apollo.models.linear import *
from apollo.models.neighbors import *
from apollo.models.trees import *
